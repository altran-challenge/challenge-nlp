# challenge NLP
### Parse&Clean&GetBOW_UnSupervised ---> Parse rdf files, clean the autheur and the abstract and extract BOW from the abstract. ALso it extract the BOW from the title&abstract and save all in pickle file 
### ML_Unsupervised_Kmeans_Abstract ---> load the cleaned dataFrame from pickle file (df_Abstract_Unsupervised), extract the 200 most frequent keywords, calculate thier tf-idf and train Kmeans and cluster the articles according to that
### Parse&CleanRDF_Supervised --->  Parse rdf files, extract the collectons (classes), clean the autheur and the abstract and extract BOW from the abstract. ALso it extract the BOW from the title&abstract and save all in pickle file 
### ML_Supervised_Abstract --->  load the cleaned dataFrame from pickle file (df_Abstract_supervised), extract the 200 most frequent keywords, calculate thier tf-idf and train Randomforest and SVM,linearSVc, ClusterCentroïds models on them
### ML_Supervised_AbstractTitle ---> load the cleaned dataFrame from pickle file (df_AbstractTitle_supervised), extract the 200 most frequent keywords, calculate thier tf-idf and train Randomforest and SVM models, linearSVc, ClusterCentroïds on them
